//
//  ProtocolDemo.m
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ProtocolDemo.h"

@interface ProtocolDemo () {
    NSInteger timesCalled;
}

@end

@implementation ProtocolDemo

- (instancetype)init
{
    if (self = [super init]) {
        timesCalled = 0;
    }
    return self;
}

- (void)startItUp
{
    if (self.delegate) {
        [self.delegate startedStuff:self];
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 1; i <= 4; i++) {
            NSTimer *t = [NSTimer scheduledTimerWithTimeInterval:i target:self selector:@selector(progressCallback) userInfo:nil repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
        }
        [[NSRunLoop currentRunLoop] run];
    });
    [NSThread sleepForTimeInterval:5.0f];
    if (self.delegate) {
        [self.delegate finishedStuff:self];
    }
}

- (void)progressCallback
{
    timesCalled++;
    if (self.delegate && [self.delegate respondsToSelector:@selector(madeProgress:toPercentage:)]) {
        [self.delegate madeProgress:self
                       toPercentage:[NSDecimalNumber decimalNumberWithDecimal:[[NSNumber numberWithDouble:timesCalled / 5.0] decimalValue]]];
    }
}

@end
