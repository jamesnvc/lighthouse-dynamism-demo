//
//  main.m
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProtocolDemo.h"
#import "ProtocolConsumer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Protocol demo
        ProtocolDemo *demo = [[ProtocolDemo alloc] init];
        ProtocolConsumer *consumer = [[ProtocolConsumer alloc] init];
        demo.delegate = consumer;
        [demo startItUp];
    }
    return 0;
}
