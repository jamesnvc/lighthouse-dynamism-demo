//
//  ProtocolDemo.h
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ProtocolDemo;

@protocol WhatsGoingOnDelegate <NSObject>

@required
- (void)startedStuff:(ProtocolDemo *)obj;
- (void)finishedStuff:(ProtocolDemo *)obj;

@optional
- (void)madeProgress:(ProtocolDemo *)obj toPercentage:(NSDecimalNumber *)percentage;

@end

@interface ProtocolDemo : NSObject

@property (nonatomic, weak) id<WhatsGoingOnDelegate> delegate;

- (void)startItUp;

@end
