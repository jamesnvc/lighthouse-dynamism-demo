//
//  ProtocolConsumer.h
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProtocolDemo.h"

@interface ProtocolConsumer : NSObject<WhatsGoingOnDelegate>

@end
