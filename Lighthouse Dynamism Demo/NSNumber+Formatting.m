//
//  NSNumber+Formatting.m
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "NSNumber+Formatting.h"

@implementation NSNumber (Formatting)

- (NSString *)percentageString
{
    NSNumberFormatter *fmter = [[NSNumberFormatter alloc] init];
    fmter.numberStyle = NSNumberFormatterPercentStyle;
    return [fmter stringFromNumber:self];
}

+ (instancetype)numberWithPercentageString:(NSString *)percent
{
    NSNumberFormatter *fmter = [[NSNumberFormatter alloc] init];
    fmter.numberStyle = NSNumberFormatterPercentStyle;
    return [fmter numberFromString:percent];
}

// [NSNumber numberWithPercantageString:@"20%"] => 

@end
