//
//  ProtocolConsumer.m
//  Lighthouse Dynamism Demo
//
//  Created by James Cash on 06-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ProtocolConsumer.h"
#import "NSNumber+Formatting.h"

@implementation ProtocolConsumer

- (void)startedStuff:(ProtocolDemo *)obj
{
    NSLog(@"Started...");
}

- (void)finishedStuff:(ProtocolDemo *)obj
{
    NSLog(@"Done!\n");
}

- (void)madeProgress:(ProtocolDemo *)obj toPercentage:(NSDecimalNumber *)percentage
{
//    NSNumberFormatter *fmter = [[NSNumberFormatter alloc] init];
//    fmter.numberStyle = NSNumberFormatterPercentStyle;
//    NSLog(@"...%@...", [fmter stringFromNumber:percentage]);
    NSLog(@"...%@...", [percentage percentageString]);
}

@end
